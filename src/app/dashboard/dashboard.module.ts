import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {DashboardPageRoutingModule} from './dashboard-routing.module';

import {DashboardPage} from './dashboard.page';
import {UpdateProfileOnceComponent} from './Components/update-profile-once/update-profile-once.component';
import {UserProfileComponent} from './page/home/user-profile/user-profile.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
  ],
  declarations: [DashboardPage, UpdateProfileOnceComponent, UserProfileComponent]
})
export class DashboardPageModule {}
