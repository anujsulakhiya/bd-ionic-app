import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardPage} from './dashboard.page';
import {UpdateProfileOnceComponent} from './Components/update-profile-once/update-profile-once.component';
import {DashboardGuard} from '../guards/dashboard.guard';
import {UpdateProfileGuard} from '../guards/update-profile.guard';

const routes: Routes = [
  {
    path: '', component: DashboardPage, children: [
      {
        path:'',
        redirectTo:'tabs',
        pathMatch:'full'
      },
      {
        canActivate:[DashboardGuard],
        path: 'tabs',
        loadChildren: () => import('../Dashboard/page/tabs/tabs.module').then( m => m.TabsPageModule)
      },

      {
        canActivate:[UpdateProfileGuard],
        path:'UpdateProfileOnce',
        component:UpdateProfileOnceComponent
      },
      // {
      //   canActivate:[DashboardGuard],
      //   path:'UserProfile/:id',
      //   component:UserProfileComponent
      // },
      {
        canActivate:[DashboardGuard],
        path: 'admin',
        loadChildren: () => import('../Dashboard/page/admin/admin.module').then( m => m.AdminPageModule)
      }
    ]
  },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
