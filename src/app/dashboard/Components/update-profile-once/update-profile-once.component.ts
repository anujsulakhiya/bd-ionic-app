import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserProfileService} from '../../page/user-profile/user-profile.service';
import {SessionService} from '../../../Services/session.service';
import {UserInterface} from '../../../Public/public.interface';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-update-profile-once',
  templateUrl: './update-profile-once.component.html',
  styleUrls: ['./update-profile-once.component.scss'],
})
export class UpdateProfileOnceComponent {

  updateProfileForm: FormGroup|any ;
  showError = false;
  emailError = false;
  dobError = false;
  user: UserInterface;
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserProfileService,
              private sessionService: SessionService) {
    this.initializeForm();
    this.user = this.sessionService.getUser();
  }

  initializeForm(){
    this.updateProfileForm = this.formBuilder.group({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      blood_group_id: ['', [Validators.required]],
      email: ['',[Validators.nullValidator,Validators.email]],
      dob: ['', [Validators.required]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      pin_code: ['', [Validators.required]],
    });
  }

  updateProfile(){
    this.isLoading = true;
    this.emailError = this.dobError = this.showError = false;
    if ( this.updateProfileForm.valid ){
      this.updateProfileForm.value.name = this.user.name;
      this.updateProfileForm.value.contact_no = this.user.contact_no;
      this.userService.updateProfile(this.updateProfileForm.value,this.user.id).subscribe( res => {
        this.sessionService.setItem('user',JSON.stringify(res));
        this.isLoading = false;
        this.sessionService.navigate('/Dashboard');
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if (err.error.errors.email) {
            this.emailError = true;
          } else if (err.error.errors.dob) {
            this.dobError = true;
          } else {
            this.showError = true;
          }
          this.isLoading = false;
        }
      });
    }
  }
}
