import {Component} from '@angular/core';
import {UserActivityInterface, UserInterface} from "../../../../Public/public.interface";
import {SessionService} from "../../../../Services/session.service";
import {ActivatedRoute} from "@angular/router";
import {UserProfileService} from "../../user-profile/user-profile.service";

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.scss'],
})
export class ShowUserComponent {


  user: UserInterface = {
    id: 0,
    name: '',
    email: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    blood_group: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    blood_group_id: 0,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    contact_no: 0,
    dob: '',
    address: '',
    city: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    pin_code: 0,
  };
  // eslint-disable-next-line @typescript-eslint/naming-convention
  user_id = 0;
  viewContact = false;
  userData: UserActivityInterface|any = {};
  userDataId = 0;
  isLoading = true;
  age;
  showAge;
  constructor(private userService: UserProfileService,
              private sessionService: SessionService,
              private route: ActivatedRoute) {
    this.getRouteParam();
  }

  getRouteParam() {
    this.route.params.subscribe(params => {
      this.user_id = params.id;
      this.getUser(params.id);
    });
  }

  getUser(id: number){
    this.isLoading = true;
    this.userService.getUser(id).subscribe( res => {
      this.isLoading = false;
      this.user = res;
    });
  }



}
