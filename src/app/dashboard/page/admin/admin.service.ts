import { Injectable } from '@angular/core';
import {HttpService} from '../../../Services/http.service';
import {Observable} from 'rxjs';
import {UserActivityInterface} from '../../../Public/public.interface';
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpService: HttpService) { }

  getUserActivity(page: number): Observable<UserActivityInterface>{
    const params = new HttpParams().set('page',page);
    return this.httpService.getWithParams('/user_activity',params);
  }
}
