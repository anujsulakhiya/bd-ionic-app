import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AdminPage} from './admin.page';
import {ShowUserComponent} from "./show-user/show-user.component";

const routes: Routes = [
  {
    path: '',
    component: AdminPage
  },
  {
    path: 'ShowUserProfile/:id', component: ShowUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule {}
