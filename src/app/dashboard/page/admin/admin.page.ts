import { Component, OnInit } from '@angular/core';
import {AdminService} from './admin.service';
import {UserActivityInterface} from '../../../Public/public.interface';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage {

  userActivity: UserActivityInterface|any =[];
  userActivityData = [];
  page = 1;
  isLoading = false;
  noActivityFound = false;
  constructor(private adminService: AdminService) {
    this.addUserActivity(this.page);
  }


  loadData(event){
    this.addUserActivity(this.page);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  addUserActivity(page: number){
    this.page = page;
    this.isLoading = true;
    this.adminService.getUserActivity(this.page).subscribe( res => {
      this.userActivity = res;
      this.isLoading = false;
      if (this.page === 1) {
        this.userActivityData = [];
      }
      if(!(this.userActivity.data.length > 0)){
        this.noActivityFound = true;
      }
      // eslint-disable-next-line @typescript-eslint/prefer-for-of
      for (let i = 0; i < this.userActivity.data.length; i++) {
        this.userActivityData.push(this.userActivity.data[i]);
      }
      this.page++;
    });
  }

  doRefresh(event) {
    this.page = 1;
    this.addUserActivity(this.page);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }


}
