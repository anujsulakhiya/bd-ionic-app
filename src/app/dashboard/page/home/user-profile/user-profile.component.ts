import {Component, Input, OnInit} from '@angular/core';
import {UserActivityInterface, UserInterface} from '../../../../Public/public.interface';
import {UserProfileService} from '../../user-profile/user-profile.service';
import {SessionService} from '../../../../Services/session.service';
import {ModalController, Platform} from "@ionic/angular";


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit{

  user_id = 0;
  @Input() user: any;
  viewContact = false;
  userData: UserActivityInterface|any = {};
  userDataId = 0;
  age;
  showAge;

  constructor(private userService: UserProfileService,
              private sessionService: SessionService,
              public modalController: ModalController,
              public platform: Platform) {

  }

  ngOnInit() {
    this.getUser(this.userData);
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  getUser(data:UserInterface){
    this.user = data;
    this.createUserActivity();
  }

  createUserActivity(){
    const currentUser = this.sessionService.getUser();
    // eslint-disable-next-line @typescript-eslint/naming-convention
    this.userData.user_id = currentUser.id;
    this.userData.user_name = currentUser.name;
    this.userData.user_contact_no = currentUser.contact_no;
    this.userData.user_blood_group = currentUser.blood_group;

    // eslint-disable-next-line @typescript-eslint/naming-convention
    this.userData.searched_user_id =  this.user.id;
    this.userData.searched_user_name = this.user.name;
    this.userData.searched_user_contact_no = this.user.contact_no;
    this.userData.searched_user_blood_group = this.user.blood_group;

    this.userService.setUserActivity(this.userData).subscribe( res =>{
      this.userDataId = res.id;
      this.viewContact = true;
    });

  }


}
