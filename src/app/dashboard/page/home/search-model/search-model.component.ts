import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-search-model',
  templateUrl: './search-model.component.html',
  styleUrls: ['./search-model.component.scss'],
})
export class SearchModelComponent {
// eslint-disable-next-line @typescript-eslint/naming-convention
  blood_group_id: number;

  constructor(public modalController: ModalController) { }
// eslint-disable-next-line @typescript-eslint/naming-convention
  setBloodGroupId(blood_group_id: number) {
    this.modalController.dismiss({
      // eslint-disable-next-line  @typescript-eslint/naming-convention
      blood_group_id,
      dismissed: true
    });
  }
}
