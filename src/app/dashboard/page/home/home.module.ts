import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import {TabsPageModule} from '../tabs/tabs.module';
import {SearchModelComponent} from './search-model/search-model.component';
import {DashboardPageModule} from '../../dashboard.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        TabsPageModule,
        DashboardPageModule
    ],
  declarations: [HomePage , SearchModelComponent]
})
export class HomePageModule {}
