import {Component, Injectable} from '@angular/core';
import {SessionService} from '../../../Services/session.service';
import {UserActivityInterface, UserInterface} from '../../../Public/public.interface';
import {UserProfileService} from '../user-profile/user-profile.service';
import {AlertController, LoadingController, ModalController, NavController, Platform} from '@ionic/angular';
import {SearchModelComponent} from './search-model/search-model.component';
import {UserProfileComponent} from "./user-profile/user-profile.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
@Injectable()
export class HomePage{

  public home: any = HomePage;
  user: UserInterface[]|any = [];
  userProfile: UserInterface|any;
  userModelData: UserInterface|any;
  noUserFound= false;
  isLoading = true;
  filter = false;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  blood_group_id = 0;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  blood_group_id_backup = 0;
  userData = [];
  page = 1;
  viewContact = false;
  userActivityData: UserActivityInterface|any = {};
  userDataId = 0;
  selectedUser: UserInterface|any;
  subscription:any;

  constructor(private loadingController: LoadingController,
              private sessionServices: SessionService,
              private userService: UserProfileService,
              private modalController: ModalController,
              public alertController: AlertController,
              private navController: NavController,
              public platform: Platform) {
    this.isProfileUpdated();
    this.addMoreUsers(this.page,false);

  }

  exitApp(){
    this.subscription = this.platform.backButton.subscribe(()=>{
      navigator['app'].exitApp();
    });
  }


  ionViewDidEnter(){
    this.exitApp();
  }

  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

  isProfileUpdated(){
    this.userService.myProfile().subscribe( res => {
      this.userProfile = res;
      if(this.userProfile.is_donor == 0){
        this.promoteDonorConfirmMessage();
      }
      if(!this.userProfile.blood_group_id){
          this.sessionServices.navigate('/Dashboard/UpdateProfileOnce');
      }
    });
  }

  async promoteDonorConfirmMessage() {

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Donate Blood Save Lives',
      message: '"Opportunities knock the door sometimes, so don’t let it go and donate blood."',
      buttons: [
        {
          text: 'Do it Later',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Confirm yourself as a Donor',
          handler: () => {

            this.navController.navigateRoot(`/Dashboard/tabs/UserProfile`);
          }
        }
      ]
    });
    await alert.present();
  }


  async presentModal() {

    this.subscription.unsubscribe();

    const modal = await this.modalController.create({
      showBackdrop: true,
      component: SearchModelComponent,
      cssClass: 'small-modal',
      backdropDismiss: true
    });

    modal.onDidDismiss().then((modelData) => {
      this.exitApp();
      if (modelData.data.blood_group_id !== null) {
        this.filter = true;
        this.page = 1;
        this.blood_group_id = this.blood_group_id_backup =  modelData.data.blood_group_id;
        this.addMoreUsersByBloodGroup(this.page,this.blood_group_id,false);
      }
    });

    return await modal.present();
  }

  async userProfileModel(user_id:number) {

    this.subscription.unsubscribe();
    this.userModelData = [];

    const userModelData = this.userData.find((id) => {
      return id.id == user_id;
    });

    const modal = await this.modalController.create({
      showBackdrop: true,
      component: UserProfileComponent,
      backdropDismiss: true,
      componentProps: {
        userData: userModelData
      }
    });

    modal.onDidDismiss().then(() => {
      this.exitApp();
    });

    return await modal.present();
  }

  loadData(event){
    this.addMoreUsers(this.page,false);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  loadDataOfBloodGroup(event){
    this.addMoreUsersByBloodGroup(this.page,this.blood_group_id,false);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  addMoreUsers(page: number,doRefresh: boolean){

    this.filter =  this.noUserFound = false;
    this.page = page;
    this.isLoading = true;
    this.userService.getAllUsers(page).subscribe( res => {
      this.user = res;

        this.isLoading = false;
        if (this.page === 1) {
          this.userData = [];
        }
        if (doRefresh) {
          this.userData = [];
        }
      if(!(this.user.data.length > 0)){
        this.noUserFound = true;
      }
      // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < this.user.data.length; i++) {
          this.userData.push(this.user.data[i]);
        }
        this.page++;
    });
  }
  // eslint-disable-next-line @typescript-eslint/naming-convention
  addMoreUsersByBloodGroup(page: number,blood_group_id: number,doRefresh: boolean){
    this.noUserFound = false;
    this.page = page;
    this.filter = true;
    this.userService.getUsersByBloodGroupId(page,blood_group_id).subscribe( res => {

      if(!(this.blood_group_id_backup === blood_group_id) || this.page === 1){
        this.userData = [];
      }
      this.user = [];
      this.user = res;

        this.isLoading = false;
        if(doRefresh){this.user = [];}
        if(!(this.user.data.length > 0)){
          this.noUserFound = true;
        }
      // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < this.user.data.length; i++) {
          this.userData.push(this.user.data[i]);
        }
        this.page++;
    });
  }

  doRefresh(event:any) {
    this.page = 1;
    this.addMoreUsers(this.page,true);
    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

}
