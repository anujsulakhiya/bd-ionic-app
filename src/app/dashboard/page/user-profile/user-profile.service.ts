import { Injectable } from '@angular/core';
import {HttpService} from '../../../Services/http.service';
import {
  ChangePasswordInterface, DonorStatusInterface, UserActivityInterface,
  UserInterface
} from '../../../Public/public.interface';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(private httpService: HttpService) { }

  updateProfile(data: UserInterface,id: number): Observable<UserInterface>{
    return this.httpService.put('/user/'+id,data);
  }

  myProfile(): Observable<UserInterface>{
    return this.httpService.get('/my_profile');
  }

  changePassword(data: ChangePasswordInterface): Observable<UserInterface>{
    return this.httpService.patch('/change_password',data);
  }

  getUser(id: number): Observable<UserInterface>{
    return this.httpService.get('/user/'+id);
  }

  getAllUsers(page: number): Observable<UserInterface[]>{
    const params = new HttpParams().set('page',page);
    return this.httpService.getWithParams('/user',params);
  }
// eslint-disable-next-line @typescript-eslint/naming-convention
  getUsersByBloodGroupId(page: number,blood_group_id: any): Observable<UserInterface[]>{
    const params = new HttpParams().set('page',page).set('blood_group_id',blood_group_id);
    return this.httpService.getWithParams('/user',params);
  }

  setUserActivity(data: UserActivityInterface): Observable<any>{
    return this.httpService.put('/create_data',data);
  }

  setDonorStatus(id: number,status: DonorStatusInterface){
    return this.httpService.patch('/set_donor_status/'+id,status);
  }
}
