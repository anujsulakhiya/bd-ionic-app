import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserInterface} from '../../../../Public/public.interface';
import {UserProfileService} from '../user-profile.service';
import {SessionService} from '../../../../Services/session.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss'],
})
export class UpdateProfileComponent {

  updateProfileForm: FormGroup|any ;
  showError = false;
  contactNoError = false;
  emailError = false;
  dobError = false;
  user: UserInterface = {
    id: 0,
    name: '',
    email: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    blood_group: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    blood_group_id: 0,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    contact_no: 0,
    dob: '',
    address: '',
    city: '',
    // eslint-disable-next-line @typescript-eslint/naming-convention
    pin_code: 0,
  };

  constructor(public modalController: ModalController,
              private formBuilder: FormBuilder,
              private userService: UserProfileService,
              private sessionService: SessionService) {
    this.getUser();
    this.initializeForm(this.user);
  }


  dismiss() {
    this.modalController.dismiss({
      dismissed: true
    });
  }

  getUser(){
    this.userService.myProfile().subscribe( res => {
      this.user = res;
      this.initializeForm(this.user);
    });
  }

  initializeForm(user: UserInterface){
    this.updateProfileForm = this.formBuilder.group({
      name: [user.name, [Validators.required]],
      email: [user.email, [Validators.nullValidator,Validators.email]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      blood_group_id: [user.blood_group_id, [Validators.required]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      contact_no: [user.contact_no, [Validators.required,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      dob: [user.dob, [Validators.required]],
      address: [user.address, [Validators.required]],
      city: [user.city, [Validators.required]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      pin_code: [user.pin_code, [Validators.required]],
    });
  }

  updateProfile(){
    this.contactNoError = this.emailError = this.dobError = this.showError = false;
    if ( this.updateProfileForm.valid ){
      this.userService.updateProfile(this.updateProfileForm.value,this.user.id).subscribe( res => {
        this.sessionService.setItem('user',JSON.stringify(res));
        this.dismiss();
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if (err.error.errors.contact_no) {
            this.contactNoError = true;
          } else if (err.error.errors.email) {
            this.emailError = true;
          } else if (err.error.errors.dob) {
            this.dobError = true;
          } else {
            this.showError = true;
          }
        }
      });
    }
  }

}
