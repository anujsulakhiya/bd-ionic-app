import {Component, OnInit} from '@angular/core';
import {DonorStatusInterface, UserInterface} from '../../../Public/public.interface';
import {AlertController, ModalController, NavController, Platform} from '@ionic/angular';
import {SessionService} from '../../../Services/session.service';
import {UserProfileService} from './user-profile.service';
import {UpdateProfileComponent} from './update-profile/update-profile.component';
import {ChangePasswordComponent} from './change-password/change-password.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  user: UserInterface[]|any = [];
  donorStatus: DonorStatusInterface = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    is_donor: false,
  };
  isAdmin = false;
  isLoading = true;
  msg = '';
  subscription:any;

  constructor(public modalController: ModalController,
              public alertController: AlertController,
              private sessionService: SessionService,
              private userService: UserProfileService,
              private navController: NavController,
              public platform: Platform) {
    this.getUser();
  }

  ngOnInit() {}


  ionViewDidEnter(){
    this.getUser();
    this.subscription = this.platform.backButton.subscribe(()=>{
      this.navController.navigateRoot(`/Dashboard/tabs/home-page`);
    });
  }

  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

  async updateProfileModel() {
    const modal = await this.modalController.create({
      component: UpdateProfileComponent,
      cssClass: 'my-custom-class'
    });
    modal.onDidDismiss().then( () => {
      this.isLoading = true;
      this.getUser();
    });
    return await modal.present();
  }

  async changePasswordModel() {
    const modal = await this.modalController.create({
      component: ChangePasswordComponent,
      cssClass: 'my-custom-class'
    });

    return await modal.present();
  }

  async donorConfirmMessage(event:any) {

    if(event.target.checked == true){
      this.msg = 'If you disable this then <strong>Please enable it when you are available to donate again</strong>';
    } else {
      this.msg = 'If you enable yourself as a Donor then <strong>Please be available whenever someone call you </strong>';
    }

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: this.msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.donorStatus.is_donor = event.target.checked == false;
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.setDonorStatus(event);
          }
        }
      ]
    });
    await alert.present();
  }

  async thankYouMessage() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Thank You',
      subHeader: 'You enable yourself as a donor',
      message: 'There is no way to put into words how much your blood donation means. ' +
        'To the emergency team tirelessly working around the clock. To the loved ones anxiously ' +
        'waiting for good news. To the patient desperate to recover. To them, your blood donation' +
        ' means everything.<strong> Thank you for being a part of our community!</strong>.',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  setDonorStatus(event){
    this.donorStatus.is_donor = event.target.checked;
    this.userService.setDonorStatus(this.user.id,this.donorStatus).subscribe( () => {
        if(this.donorStatus.is_donor == true){
          this.thankYouMessage();
        }
    }, () => {
      this.donorStatus.is_donor = !event.target.checked;
    });
  }

  getUser(){
    this.userService.myProfile().subscribe( res =>{
      this.user = res;
      this.isLoading = false;
      this.isAdmin = this.user.is_admin === 1;

      this.donorStatus.is_donor = this.user.is_donor !== 0;
    });
  }

  doLogout(){
    this.sessionService.destroySession();
  }


}
