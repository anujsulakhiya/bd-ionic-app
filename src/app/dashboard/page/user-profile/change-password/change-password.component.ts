import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {HttpErrorResponse} from '@angular/common/http';
import {UserProfileService} from '../user-profile.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent {

  changePassword: FormGroup|any ;
  changePasswordSuccess = false;
  confirmPasswordInvalid = false;
  newPasswordError = false;
  currentPasswordError = false;
  showError = false;

  constructor(private formBuilder: FormBuilder,
              public modalController: ModalController,
              private userService: UserProfileService) {
    this.initializeForm();
  }

  dismiss() {
    this.modalController.dismiss({
      dismissed: true
    });
  }

  initializeForm(){
    this.changePassword = this.formBuilder.group({
      password: ['', [Validators.required,Validators.minLength(6)]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      new_password: ['', [Validators.required,Validators.minLength(6)]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      confirm_new_password: ['', [Validators.required]]
    });
  }

  doChangePassword(){
    this.confirmPasswordInvalid = this.currentPasswordError = this.newPasswordError = this.showError =  false;
    if ( this.changePassword.valid ){
      this.userService.changePassword(this.changePassword.value).subscribe( res => {
        this.changePasswordSuccess = true;
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if (err.error.errors.password) {
            this.currentPasswordError = true;
          } else if (err.error.errors.new_password) {
            this.newPasswordError = true;
          } else if (err.error.errors.confirm_password) {
            this.confirmPasswordInvalid = true;
          } else {
            this.showError = true;
          }
        }
      });
    }
  }

}
