import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UpdateProfileComponent} from './update-profile/update-profile.component';
import {UserProfilePage} from './user-profile.page';

const routes: Routes = [

  {
    path: '', component: UserProfilePage, children: [
      {
        path:'', redirectTo:'MyProfile'
      },
      {
        path: 'MyProfile',
        component: UserProfilePage
      },
      {
        path: 'UpdateProfile',
        component: UpdateProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserProfilePageRoutingModule {}
