import { Injectable } from '@angular/core';
import {HttpService} from "../../../Services/http.service";
import {Observable} from "rxjs";
import {AboutUsInterface} from "../../../Public/public.interface";

@Injectable({
  providedIn: 'root'
})
export class AboutUsService {

  constructor(private httpService: HttpService) { }

  getAboutUsDetails(): Observable<AboutUsInterface[]>{
    return this.httpService.get('/aboutUsPageDetails');
  }
}
