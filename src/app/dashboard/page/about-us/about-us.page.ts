import {Component} from '@angular/core';
import {AboutUsInterface} from "../../../Public/public.interface";
import {AboutUsService} from "./about-us.service";
import {NavController, Platform} from "@ionic/angular";

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage {

  aboutUs: AboutUsInterface|any = [];
  subscription:any;

  constructor(private aboutUsService: AboutUsService,
              private navController: NavController,
              public platform: Platform) {
    this.getAboutUsDetails();
  }
  ionViewDidEnter(){
    this.getAboutUsDetails();
    this.subscription = this.platform.backButton.subscribe(()=>{
      this.navController.navigateRoot(`/Dashboard/tabs/home-page`);
    });
  }

  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

  getAboutUsDetails(){
    this.aboutUsService.getAboutUsDetails().subscribe( res => {
      this.aboutUs = res;
    })
  }

}
