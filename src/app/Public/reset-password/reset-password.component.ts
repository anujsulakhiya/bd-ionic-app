import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublicService} from '../public.service';
import {SessionService} from '../../Services/session.service';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent {

  resetPassword: FormGroup|any ;
  showError = false;
  resetPasswordSuccess = false;
  confirmPasswordInvalid = false;
  userId = 0;

  constructor(private formBuilder: FormBuilder,
              private publicService: PublicService,
              private sessionService: SessionService,
              private route: ActivatedRoute) {
    this.getRouteParam();
    this.initializeForm();
  }

  getRouteParam() {
    this.route.params.subscribe(params => {
      this.userId = params.id;
    });
  }

  initializeForm(){
    this.resetPassword = this.formBuilder.group({
      password: ['', [Validators.required,Validators.minLength(6)]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      confirm_password: ['', [Validators.required,]]
    });
  }

  resetUserPassword(){
    this.confirmPasswordInvalid = this.showError = false;
    if ( this.resetPassword.valid ){
      this.publicService.resetPassword(this.resetPassword.value,this.userId).subscribe( () => {
            this.resetPasswordSuccess = true;
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if (err.error.errors.confirm_password) {
            this.confirmPasswordInvalid = true;
          } else {
            this.showError = true;
          }
        }
      });
    }
  }

}
