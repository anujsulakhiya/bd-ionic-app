import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublicService} from '../public.service';
import {SessionService} from '../../Services/session.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.scss'],
})
export class VerifyUserComponent {

  verifyUser: FormGroup|any ;
  showError = false;
  dobError = false;
  contactNoError = false;

  constructor(private formBuilder: FormBuilder,
              private publicService: PublicService,
              private sessionService: SessionService) {
    this.initializeForm();
  }

  initializeForm(){
    this.verifyUser = this.formBuilder.group({
      // eslint-disable-next-line @typescript-eslint/naming-convention
      contact_no: ['', [Validators.required,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      dob: ['', [Validators.required]]
    });
  }

  verifyUserDetails(){
    this.showError = this.dobError = this.contactNoError = false;
    if ( this.verifyUser.valid ){
      this.publicService.verifyUser(this.verifyUser.value).subscribe( res => {
        if(res.id){
          this.sessionService.navigate('/Public/ResetPassword/'+res.id);
        }
      }, (err) => {
        this.showError = true;

        if (err instanceof HttpErrorResponse) {

          if (err.error.errors.contact_no) {

            this.contactNoError = true;

          } else if (err.error.errors.dob) {

            this.dobError = true;

          } else {

            this.showError = true;
          }
        }


      });
    }
  }
}
