import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {PublicPage} from './public.page';
import {VerifyUserComponent} from './verify-user/verify-user.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {PublicGuard} from '../guards/public.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'Home',
    pathMatch: 'full'
  },
  {
    canActivate:[PublicGuard],
    path: 'Home',
    component: PublicPage
  },
  {
    canActivate:[PublicGuard],
    path: 'Login',
    component: LoginComponent
  },
  {
    canActivate:[PublicGuard],
    path: 'Register',
    component: RegisterComponent
  },
  {
    canActivate:[PublicGuard],
    path: 'VerifyUser',
    component: VerifyUserComponent
  },
  {
    canActivate:[PublicGuard],
    path: 'ResetPassword/:id',
    component:ResetPasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicPageRoutingModule {}
