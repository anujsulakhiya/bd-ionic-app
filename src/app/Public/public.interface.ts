
export interface LoginInterface{
  // eslint-disable-next-line @typescript-eslint/naming-convention
  contact_no: string;
  password: string;
}

export interface RegisterInterface{
  name: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  contact_no: string;
  password: string;
}

export interface VerifyUserInterface{
  // eslint-disable-next-line @typescript-eslint/naming-convention
  contact_no: string;
  dob: string;
}

export interface UserInterface {
  id: number;
  name: string;
  email: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  blood_group: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  blood_group_id: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  contact_no: number;
  dob: string;
  address: string;
  city: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  pin_code: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_profile_updated?: boolean;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_donor?: boolean;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_admin?: boolean;
}

export interface LoginSuccessInterface {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  access_token: string;
  user: UserInterface;
}

export interface GoogleAccessTokenInterface{
  accessToken: string;
  expires: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  expires_in: number;
  email: string;
  userId: string;
  displayName: string;
  familyName: string;
  givenName: string;
}

export interface GoogleAuthRequestInterface {
  idToken: string;
}


export interface DonorStatusInterface{
  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_donor: boolean;
}

export interface ChangePasswordInterface {
  password: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  new_password: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  confirm_new_password: string;
}

export interface UserActivityInterface{
  // eslint-disable-next-line @typescript-eslint/naming-convention
  user_id: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  user_name: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  user_contact_no: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  user_blood_group: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  searched_user_id: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  searched_user_name: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  searched_user_contact_no: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  searched_user_blood_group: string;
}

export interface AboutUsInterface {
  heading: string
  description: string
  owner: string;
  contact: string;
  mission: string;
  vision: string;
}
