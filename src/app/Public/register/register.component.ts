import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublicService} from '../public.service';
import {SessionService} from '../../Services/session.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {

  registerForm: FormGroup|any ;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  contact_no_error = false;
  showError = false;
  isLoading = false;

  constructor(private formBuilder: FormBuilder,
              private publicService: PublicService,
              private sessionService: SessionService) {
    this.initializeForm();
  }


  initializeForm(){
    this.registerForm = this.formBuilder.group({
      name: ['',[Validators.required]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      contact_no: ['', [Validators.required,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  doRegister(){
    this.isLoading = true;
    this.contact_no_error = this.showError = false;
    if ( this.registerForm.valid ){
      this.publicService.register(this.registerForm.value).subscribe( res => {
        this.sessionService.setSession(res);
        this.isLoading = false;
      }, (err) => {
        if (err instanceof HttpErrorResponse && err.error.errors.contact_no) {
          this.contact_no_error = true;
        } else {
          this.showError = true;
          this.isLoading = false;
        }
      });
    }
  }

  removeError(){
    this.contact_no_error = false;
  }

}
