import { Injectable } from '@angular/core';
import {
  GoogleAuthRequestInterface,
  LoginInterface,
  LoginSuccessInterface,
  RegisterInterface, UserInterface,
  VerifyUserInterface
} from './public.interface';
import {Observable} from 'rxjs';
import {HttpService} from '../Services/http.service';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private httpService: HttpService) {
  }

  login(data: LoginInterface): Observable<LoginSuccessInterface> {
    return this.httpService.post('/auth/login', data);
  }

  register(data: RegisterInterface): Observable<LoginSuccessInterface> {
    return this.httpService.post('/auth/register', data);
  }

  verifyUser(data: VerifyUserInterface): Observable<UserInterface> {
    return this.httpService.put('/auth/verify_user', data);
  }

  resetPassword(data: VerifyUserInterface,id: number): Observable<UserInterface> {
    return this.httpService.put('/auth/reset_password/'+id, data);
  }

  doGoogleLogin(authData: GoogleAuthRequestInterface): Observable<LoginSuccessInterface> {
    return this.httpService.post('auth/google_login', authData);
  }
}
