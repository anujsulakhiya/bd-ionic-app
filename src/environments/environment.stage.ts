const url = 'https://staging-bd.ossoftindia.com';
const apiUrl =  url + '/api';


export const environment = {
  production: true,
  appUrl: url,
  apiUrl
};
